<?php
/**
 * @category    Scandiweb
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\ABTesting\Plugin;

use Exception;
use Magento\Framework\View\Page\Config;
use Psr\Log\LoggerInterface;
use Scandiweb\ABTesting\Helper\Configurations;

class ConfigPlugin
{
    /**
     * @var Configurations
     */
    protected $configurations;

    /**
     * Class attached to html for cloaking it while
     * tests are loading
     */
    const ASYNC_HIDE_CLASS = 'async-hide';

    /**
     * RendererPlugin constructor.
     *
     * @param Configurations $configurations
     * @param LoggerInterface $logger
     */
    public function __construct(
        Configurations $configurations,
        LoggerInterface $logger
    ) {
        $this->configurations = $configurations;
        $this->logger = $logger;
    }

    /**
     * If async hide aka initialisation cloak is enabled
     * add async-hide class to html element if that's not done yet
     *
     * This is indirectly used by root.phtml
     *
     * @param Config $subject
     * @param callable $proceed
     * @param $elementType
     *
     * @return string[]
     */
    public function aroundGetElementAttributes(Config $subject, callable $proceed, $elementType)
    {
        if ($elementType === Config::ELEMENT_TYPE_HTML &&
            $this->configurations->getIsEnabled() &&
            $this->configurations->getAsyncHideEnabled()) {
            $htmlClass = $subject->getElementAttribute(Config::ELEMENT_TYPE_HTML,'class') ?: '';

            // If async hide class not yet added
            if (stripos($htmlClass, static::ASYNC_HIDE_CLASS) === false) {
                // Append the class
                if ($htmlClass === '') {
                    $htmlClass .= static::ASYNC_HIDE_CLASS;
                } else {
                    $htmlClass .= ' ' . static::ASYNC_HIDE_CLASS;
                }

                try {
                    // Save change
                    $subject->setElementAttribute(Config::ELEMENT_TYPE_HTML, 'class', $htmlClass);
                } catch (Exception $exception) {
                    $this->logger->error(sprintf(
                        __("Google Optimize extension failed to add %s class to %s element"),
                        static::ASYNC_HIDE_CLASS,
                        Config::ELEMENT_TYPE_HTML
                    ));
                }
            }
        }

        return $proceed($elementType);
    }
}