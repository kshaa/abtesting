<?php
/**
 * @category    Scandiweb
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\ABTesting\Plugin;

use Exception;
use Magento\Framework\View\Element\Template;
use Scandiweb\ABTesting\Helper\Configurations;

class TemplatePlugin
{
    /**
     * @var Configurations
     */
    protected $configurations;

    /**
     * Name of the block for injecting very early head element content
     * This is hardcoded in default.xml
     */
    const HEAD_START_BLOCK = 'head.start';

    /**
     * Name of the block which is used for injecting early head content
     * Reason - In core M2 requirejs is the earliest block in the page (see root.phtml)
     */
    const REQUIREJS_BLOCK = 'require.js';

    /**
     * TemplatePlugin constructor.
     *
     * @param Configurations $configurations
     */
    public function __construct(
        Configurations $configurations
    ) {
        $this->configurations = $configurations;
    }

    /**
     * Least intrusive way of injecting custom content in early head
     *
     * @param Template $subject
     * @param $result
     *
     * @return string
     */
    public function afterToHtml(Template $subject, $result)
    {
        if ($subject->getNameInLayout() == static::REQUIREJS_BLOCK &&
            $this->configurations->getIsEnabled()) {
            try {
                $layout = $subject->getLayout();
                if (!$layout) {
                    return $result;
                }

                $headStartBlock = $layout->getBlock(static::HEAD_START_BLOCK);
                if (!$headStartBlock) {
                    return $result;
                }

                $headStartInjection = $headStartBlock->toHtml() ?: '';
                $result = $headStartInjection . ' ' . $result;
            } catch (Exception $exception) {
                return $result;
            }
        }

        return $result;
    }
}