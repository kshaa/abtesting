<?php
/**
 * @category    Scandiweb
 * @author      Scandiweb <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\ABTesting\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class LayoutLoadBeforeEvent implements ObserverInterface
{
    /**
     * Delimiter for query parameter values
     */
    const AB_QUERY_PARAM_DELIMITER = ',';

    /**
     * Query parameter name
     */
    const AB_QUERY_PARAM = 'v';

    /**
     * Layout handle name
     */
    const AB_LAYOUT_HANDLE = 'ab';

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * LayoutLoadBeforeEvent constructor.
     *
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * Before loading all layouts, check for ab test in request url
     * and load additional layouts (AB tests)
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $queryParam = $this->request->getParam(self::AB_QUERY_PARAM);

        if (!empty($queryParam)) {
            /** @var \Magento\Framework\View\Layout $layout */
            $layout = $observer->getData('layout');
            $tests = explode(self::AB_QUERY_PARAM_DELIMITER, $queryParam);

            foreach ($tests as $test) {
                $handle = $observer->getData('full_action_name') . '_' . self::AB_LAYOUT_HANDLE . '_' . $test;
                $layout->getUpdate()->addHandle($handle);
            }
        }
    }
}
