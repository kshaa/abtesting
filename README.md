# Scandiweb_ABTesting

Before all layout loading, check URL request
for AB testing query parameter and load
additional layout(s) with the AB test layout handle.

## Installation

Add Scandiweb_Core if it is not added already by running
```
composer config repositories.scandiweb-module-core git git@github.com:scandiwebcom/Scandiweb-Assets-Core.git
composer require scandiweb/module-core:~0.1.3
```

Add the extension by running

```
# Didn't test this yet
# composer config repositories.kshaa https://bitbucket.com:kshaa/abtesting.git
composer require kshaa/abtesting:dev-master
php -f bin/magento setup:upgrade
```

### Example usage
Request URL:

* http://www.example.com/customer/account/login?v=x,y

Additional layouts loaded:

* customer_account_login_ab_x.xml

* customer_account_login_ab_y.xml

### Example setup
#### Server side
To enable a test version of the login page.
Create a test layout just like you would extend/override core layouts.

E.g. create layout `customer_account_login_ab_x.xml`
```
    <?xml version="1.0"?>
    <page>
        <referenceBlock name="page.main.title" remove="true" />
    </page>
```
The result when you'll open the /customer/account/login page with query ?v=x, the page title will be removed.

#### Client side
For actually splitting half the traffic to the test version of the page you should use Google Optimize.

0. (Inject Google optimize script in page) 
1. Set up redirect test in Google Optimize
2. Target www.example.com/customer/account/login (https://i.imgur.com/3upVd6R.jpg)
2. Set up variant (page version) to redirect to the same url with a query. (https://i.imgur.com/VqRaQld.jpg)
3. Run experiment!
