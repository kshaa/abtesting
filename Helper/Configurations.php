<?php
/**
 * @package Scandiweb\ABTesting
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @author Arturs Gailis <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ABTesting\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;

class Configurations {
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var bool
     */
    protected $isEnabledGlobally;

    /**
     * Store configuration paths
     */
    const XML_PATH_IS_ENABLED = 'scandiweb_abtesting/general/enable';
    const XML_PATH_IS_ENABLED_REDIRECT_TESTS = 'scandiweb_abtesting/general/enable_redirect_tests';
    const XML_PATH_SEND_PAGEVIEW = 'scandiweb_abtesting/general/send_pageview';
    const XML_PATH_GOOGLE_ANALYTICS_ID = 'scandiweb_abtesting/general/google_analytics_id';
    const XML_PATH_GOOGLE_OPTIMIZE_ID = 'scandiweb_abtesting/general/google_optimize_id';
    const XML_PATH_ASYNC_HIDE_ENABLED = 'scandiweb_abtesting/general/async_hide_enable';
    const XML_PATH_WAIT_TIMEOUT = 'scandiweb_abtesting/general/wait_timeout';

    /**
     * Configurations constructor
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->isEnabledGlobally = $this->scopeConfig->getValue(static::XML_PATH_IS_ENABLED);
    }

    /**
     * Is extension enabled
     *
     * @return bool
     */
    public function getIsEnabled()
    {
        return (bool) $this->getConfigValue(static::XML_PATH_IS_ENABLED) ?: false;
    }

    /**
     * @return bool
     */
    public function getIsEnabledRedirectTests()
    {
        return (bool) $this->getConfigValue(static::XML_PATH_IS_ENABLED_REDIRECT_TESTS) ?: false;
    }

    /**
     * Should the pageview hit be sent through this extension
     *
     * @return bool
     */
    public function getSendPageView()
    {
        return (bool) $this->getConfigValue(static::XML_PATH_SEND_PAGEVIEW) ?: false;
    }

    /**
     * Google Analytics ID
     *
     * @return string
     */
    public function getGoogleAnalyticsId()
    {
        return (string) $this->getConfigValue(static::XML_PATH_GOOGLE_ANALYTICS_ID) ?: '';
    }

    /**
     * Google Optimize ID
     *
     * @return string
     */
    public function getGoogleOptimizeId()
    {
        return (string) $this->getConfigValue(static::XML_PATH_GOOGLE_OPTIMIZE_ID) ?: '';
    }

    /**
     * Is initial loading cloak enabled
     * (while test computation magic is happening)
     *
     * @return bool
     */
    public function getAsyncHideEnabled()
    {
        return (bool) $this->getConfigValue(static::XML_PATH_ASYNC_HIDE_ENABLED) ?: false;
    }

    /**
     * Timeout before removing blank screen
     *
     * @return int
     */
    public function getWaitTimeout()
    {
        return (int) $this->getConfigValue(static::XML_PATH_WAIT_TIMEOUT) ?: 4000;
    }

    /**
     * Get config value from table
     *
     * @param $path
     * @return mixed
     */
    public function getConfigValue($path)
    {
        if ($this->isEnabledGlobally) {
            $configValue = $this->scopeConfig->getValue($path);
        } else {
            $scopeId = $this->storeManager->getStore()->getId();
            
            $configValue = $this->scopeConfig->getValue(
                $path, "stores", $scopeId
            );
        }

        return $configValue;
    }
}