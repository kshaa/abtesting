<?php
/**
 * @package Scandiweb\ABTesting
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ABTesting\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Scandiweb\ABTesting\Helper\Configurations;

class GoogleOptimize extends Template {
    /**
     * @var Configurations
     */
    protected $configurations;

    /**
     * GoogleOptimize constructor.
     *
     * @param Context $context
     * @param Configurations $configurations
     * @param array $data
     */
    public function __construct(
        Context $context,
        Configurations $configurations,
        array $data = []
    ) {
        $this->configurations = $configurations;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getConfigurations()->getIsEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * @return Configurations
     */
    public function getConfigurations()
    {
        return $this->configurations;
    }
}